import React,{Component} from 'react';


class SearchBar extends Component{
    
    constructor(props){
        super(props);
        this.state = {
                      searchText:"",
                      placeHolder:"Saisissez un film...",
                      intervalBeforeRequest:1000,
                      lockRequest:false,
                    }
    }
    /*renderInputs(){
        const {searchText} = this.props
        if(!searchText){
          return  <div><input  type="number" value={this.state.searchText}  /> </div>
                        
        }else{
            return null;
        }
    }*/

    render(){
        return (
                <div className="row">
                <div className="col-md-8 input-group"> 
                    <input type="text" className="form-control input-lg" onChange = { this.handleChange.bind(this)} placeholder={this.state.placeHolder}/>
                    <span className="input-group-btn">
                        <button className="btn btn-secondary" onClick={this.handleOnClick.bind(this)}>Rechercher</button>
                    </span>
                 </div> 
                
                </div>
               )
    }
           
    handleChange(e){
        //console.log('----------------');
        //console.log('Vous saisissez: ',event.target.value);
        //console.log('----------------');
        this.setState({searchText:e.target.value});
        if(!this.state.lockRequest){
            this.setState({lockRequest:true});
            this.setTimeout(function(){ this.search() }.bind(this),this.state.intervalBeforeRequest)
        }
    }

    handleOnClick(){
        //console.log('Clicked',);
        this.search();
    }
    search(){
        this.props.callback(this.state.searchText);
        this.setState({lockRequest:false});
    }
}

export default SearchBar;   