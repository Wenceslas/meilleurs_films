import React from 'react';
import VideoListItem from '../components/video-list-item';


const VideoList = (props)=>{
    const {movieList} = props;
    //console.log('Voici moviList: ', movieList);
    
//const movies = ["Film1","Film2","Film3","Film4","Film5"]
    return(
        <div>
            <ul>
                {
                    movieList.map( movie=>{

                        return <VideoListItem key={movie.id} movie={movie} callback={receiveCallback} />
                        }
                  
                    )

                } 
                
                
            </ul>
        </div>
    );

    function receiveCallback(movie) {
          //console.log('Parent: ', movie);  
          props.callback(movie);
    }
}

export default VideoList;  